import pymysql


def connect_db(host, user, password, port):
    """
    Realiza conexion a un servidor MYSql, devuelve el objecto de conexion
    """
    try:
        connection = pymysql.connect(
            host=host,
            user=user,
            passwd=password,
            port=int(port),
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor
        )

        return connection

    except Exception as e:
        print("Exception ocurred:{}".format(e))
        exit()

# crea db


def create_database(connection, newdb):
    """
    Crea una base de datos en servidor MYSql. 
    Se requiere una conexion establecida y la entrada en objeto de conexion.
    """
    cursorInstance = connection.cursor()
    query = "CREATE DATABASE " + newdb

    try:
        return cursorInstance.execute(query)
    except Exception as e:
        print("Exception ocurred:{}".format(e))


# consulta DB, devuelve 1 si es true y 0 si es false
def get_db(connection, db):
    cursorInstance = connection.cursor()
    query = "SHOW DATABASES LIKE '{}'".format(db)
    try:
        return cursorInstance.execute(query)
    except Exception as e:
        print("Exception ocurred:{}".format(e))


# crea tabla
def create_table(connection, db, table):
    cursor_instance = connection.cursor()

    # selecciona db
    cursor_instance.execute("use " + db)

    # crea tabla
    query = """CREATE TABLE {} (
            id INT AUTO_INCREMENT,
            URL TEXT NOT NULL,
            user VARCHAR(60) NOT NULL,
            PRIMARY KEY (id)
            )""".format(table)
    try:
        return cursor_instance.execute(query)
    except Exception as e:
        print("Exception ocurred:{}".format(e))


def insert_value(connection, db, table, url, cookie):
    cursor_instance = connection.cursor()

    cursor_instance.execute("use " + db)
    query = "INSERT INTO {} (url, user) VALUES (%s,%s)".format(
        table)

    try:
        cursor_instance.execute(query, (url, cookie))
        id_value = cursor_instance.lastrowid
        connection.commit()
        return id_value

    except Exception as e:
        print("Exception ocurred:{}".format(e))
