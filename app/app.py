from string import ascii_uppercase
from string import ascii_lowercase
import base64
from flask import Flask, render_template, request, make_response, g, redirect
from flask_cors import CORS
import socket
import random
import re
import string
from math import floor
from modules import mysql
import os
import config as cfg

from urllib.parse import urlparse  # Python 3
str_encode = str.encode

table = 'listUrl'
try:
    mysql_server = os.environ['mysql_server']
    mysql_db = os.environ['mysql_db']
    mysql_user = os.environ['mysql_user']
    mysql_password = os.environ['mysql_password']
    mysql_port = os.environ['mysql_port']
    host = os.environ['host_url']
except Exception as e:
    mysql_server = cfg.mysql_server
    mysql_db = cfg.mysql_db
    mysql_user = cfg.mysql_user
    mysql_password = cfg.mysql_password
    mysql_port = cfg.mysql_port
    host = "http://localhost:5000/"


app = Flask(__name__)
CORS(app)

regex = re.compile(
    r'^(?:http|ftp)s?://'  # http:// or https://
    # domain...
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
    r'localhost|'  # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)


def toBase62(num, b=62):
    if b <= 0 or b > 62:
        return 0
    base = string.digits + ascii_lowercase + ascii_uppercase
    r = num % b
    res = base[r]
    q = floor(num / b)
    while q:
        r = q % b
        q = floor(q / b)
        res = base[int(r)] + res
    return res


def toBase10(num, b=62):
    base = string.digits + ascii_lowercase + ascii_uppercase
    limit = len(num)
    res = 0
    for i in range(limit):
        res = b * res + base.find(num[i])
    return res


@app.route("/", methods=['POST', 'GET'])
def home():
    value = None
    redirect_url = None
    cookies = request.cookies.get('id')

    if request.method == "POST":
        url = request.form['url']
        # encode
        if urlparse(url).scheme == '':
            url = 'http://' + url

        url_match = re.match(regex, url)
        if url_match:
            original_url = str_encode(url)

            # conexion mysql, crea db tabla
            sql_connection = mysql.connect_db(mysql_server, mysql_user,
                                              mysql_password, mysql_port)
            if (mysql.get_db(sql_connection, mysql_db) == 0):
                mysql.create_database(sql_connection, mysql_db)
            # crea tabla
            mysql.create_table(sql_connection, mysql_db, table)

            original_url = [base64.urlsafe_b64encode(original_url)]
            id_value = mysql.insert_value(
                sql_connection, mysql_db, table, original_url, cookies)
            if id_value:
                encoded_string = toBase62(id_value)

                encoded_url = encoded_string

                redirect_url = host + encoded_url
                value = 'True'
        else:
            value = None

    if cookies:
        sql_connection = mysql.connect_db(mysql_server, mysql_user,
                                          mysql_password, mysql_port)
        if (mysql.get_db(sql_connection, mysql_db) == 0):
            mysql.create_database(sql_connection, mysql_db)
            # crea tabla
            mysql.create_table(sql_connection, mysql_db, table)

        cursor = sql_connection.cursor()
        cursor.execute("use " + mysql_db)
        cursor.execute(
            "SELECT * FROM listUrl WHERE user='{}'".format(cookies))
        get_user_url = cursor.fetchall()
        if get_user_url:
            lista = []
            get_user_url.reverse()
            get_user_url = get_user_url[0:3]

            for e in get_user_url:
                a = base64.urlsafe_b64decode(e['URL']).decode("utf-8") 
                new_url = toBase62(e['id'])
                new_url = host + new_url
                lista.append((a,new_url))
        else:
            lista = None
    else:
        lista = None
        new_cookies = hex(random.getrandbits(64))[2:-1]

    resp = make_response(render_template("home.html", url=value, short_url=redirect_url, get_user_url=lista))

    if not cookies:
        resp.set_cookie('id', new_cookies)
    return resp


# make redirection
@app.route('/<short_url>')
def redirect_short_url(short_url):
    decoded = toBase10(short_url)
    url = host  # fallback if no URL is found
    sql_connection = mysql.connect_db(mysql_server, mysql_user,
                                      mysql_password, mysql_port)
    cursor = sql_connection.cursor()
    cursor.execute("use " + mysql_db)
    cursor.execute("SELECT URL FROM listUrl WHERE ID='{}'".format(decoded))
    try:
        value_fetch = cursor.fetchone()
        if value_fetch is not None:
            url = base64.urlsafe_b64decode(value_fetch['URL'])
            return redirect(url)
    except Exception as e:
        print(e)
    return redirect(url)

# error handler
@app.errorhandler(404)
def not_found(e):
    return render_template("404.html")


if __name__ == "__main__":
    app.run(host='0.0.0.0')
